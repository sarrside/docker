from django.apps import AppConfig


class MonsiteAppConfig(AppConfig):
    name = 'monsite_app'
